import { MenuService } from './../../_service/menu.service';
import { PacienteService } from './../../_service/paciente.service';
import { Paciente } from './../../_model/paciente';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-rol-to-menu',
  templateUrl: './rol-to-menu.component.html',
  styleUrls: ['./rol-to-menu.component.css']
})
export class RolToMenuComponent implements OnInit {

  lista: Paciente[] = [];
  displayedColumns = ['idMenu', 'nombre', 'icono', 'url','acciones'];
  dataSource: MatTableDataSource<Paciente>;
  cantidad : number;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private service: MenuService, private snackBar: MatSnackBar) { }

  ngOnInit() {
  
    this.service.listarMenusPageable(0, 10).subscribe(data => {
      let menus = JSON.parse(JSON.stringify(data)).content;
      this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;
      this.dataSource = new MatTableDataSource(menus);
      this.dataSource.sort = this.sort;

    })
    this.service.mensaje.subscribe(data => {        
      this.snackBar.open(data, 'Aviso', { duration: 2000 });
    }); 
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }



  mostrarMas(e: any){
    console.log(e);
    this.service.listarMenusPageable(e.pageIndex, e.pageSize).subscribe(data => {
      console.log(data);
      let pacientes = JSON.parse(JSON.stringify(data)).content;
      this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;

      this.dataSource= new MatTableDataSource(pacientes);
      
      this.dataSource.sort = this.sort;
      
    });
  }
}
