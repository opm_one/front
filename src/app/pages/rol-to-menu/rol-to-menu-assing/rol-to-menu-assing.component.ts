import { Menu } from './../../../_model/menu';
import { Rol } from './../../../_model/rol';
import { MenuService } from './../../../_service/menu.service';
import { RolService } from './../../../_service/rol.service';

import { PacienteService } from './../../../_service/paciente.service';
import { Paciente } from './../../../_model/paciente';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-rol-to-menu-assing',
  templateUrl: './rol-to-menu-assing.component.html',
  styleUrls: ['./rol-to-menu-assing.component.css']
})
export class RolToMenuAssingComponent implements OnInit {

  id: number;
  listRolOriginal:Rol[]=[];  
  listRol:Rol[]=[];
  listRolSelected:Rol[]=[];
  menuName:String;
  selectedValue;

  rolesActuales: Rol[] = [

  ];




  constructor(private service: MenuService,private rolService: RolService, private route: ActivatedRoute, private router: Router) {
  
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.listRolSelected=[];
      this.initForm();
    });
  }

  private asign(){
    let menu=new Menu();
    menu.idMenu=this.id;
    menu.roles=this.listRolSelected;
    this.service.registrarMenuInRol(menu).subscribe(data => {
      this.service.mensaje.next('Se asignaron');
      
     });

  }
  private addToAssign(){
    this.listRolSelected.push(this.listRol.find(x=>x.idRol==this.selectedValue));
    this.listRol=this.listRolOriginal.filter(x=> this.listRolSelected.find(y=>y.idRol==x.idRol)==null);
    this.selectedValue=null;

  }

  private eliminar(id){
    this.listRolSelected=this.listRolSelected.filter(x=> x.idRol!=id);    
    this.listRol=this.listRolOriginal.filter(x=> this.listRolSelected.find(y=>y.idRol==x.idRol)==null);
    
  }

  private initForm() {
      this.service.listarMenuPorId(this.id).subscribe(data => {
        this.menuName=data.nombre;
        this.rolesActuales=data.roles;
        this.rolService.listar().subscribe(data => {
          this.listRolOriginal=data.filter(x=>this.rolesActuales.find(y=>y.idRol==x.idRol)==null);
          this.listRol=this.listRolOriginal;

        });
        
      });
    


  }

  operar() {
 
  }

}
