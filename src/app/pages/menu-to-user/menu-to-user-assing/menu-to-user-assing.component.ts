import { Usuario } from './../../../_model/usuario';
import { Menu } from './../../../_model/menu';
import { Rol } from './../../../_model/rol';
import { UsuarioService } from './../../../_service/usuario.service';
import { RolService } from './../../../_service/rol.service';

import { PacienteService } from './../../../_service/paciente.service';
import { Paciente } from './../../../_model/paciente';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-menu-to-user-assing',
  templateUrl: './menu-to-user-assing.component.html',
  styleUrls: ['./menu-to-user-assing.component.css']
})
export class MenuToUserAssingComponent implements OnInit {

  id: number;
  listRolOriginal:Rol[]=[];  
  listRol:Rol[]=[];
  listRolSelected:Rol[]=[];
  menuName:String;
  selectedValue;

  rolesActuales: Rol[] = [

  ];




  constructor(private service: UsuarioService,private rolService: RolService, private route: ActivatedRoute, private router: Router) {
  
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.listRolSelected=[];
      this.initForm();
    });
  }

  private asign(){
    let user=new Usuario();
    user.idUsuario=this.id;
    user.roles=this.listRolSelected;
    this.service.registraUserInRol(user).subscribe(data => {
      this.service.mensaje.next('Se asignaron');
      
     });

  }
  private addToAssign(){
    this.listRolSelected.push(this.listRol.find(x=>x.idRol==this.selectedValue));
    this.listRol=this.listRolOriginal.filter(x=> this.listRolSelected.find(y=>y.idRol==x.idRol)==null);
    this.selectedValue=null;

  }

  private eliminar(id){
    this.listRolSelected=this.listRolSelected.filter(x=> x.idRol!=id);    
    this.listRol=this.listRolOriginal.filter(x=> this.listRolSelected.find(y=>y.idRol==x.idRol)==null);
    
  }

  private initForm() {
      this.service.listarUsuarioPorId(this.id).subscribe(data => {
        this.menuName=data.username;
        this.rolesActuales=data.roles;
        this.rolService.listar().subscribe(data => {
          this.listRolOriginal=data.filter(x=>this.rolesActuales.find(y=>y.idRol==x.idRol)==null);
          this.listRol=this.listRolOriginal;

        });
        
      });
    


  }

  operar() {
 
  }

}
