import { UsuarioService} from './../../_service/usuario.service';
import { PacienteService } from './../../_service/paciente.service';
import { Paciente } from './../../_model/paciente';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-menu-to-user',
  templateUrl: './menu-to-user.component.html',
  styleUrls: ['./menu-to-user.component.css']
})
export class MenuToUserComponent implements OnInit {

  lista: Paciente[] = [];
  displayedColumns = ['idUser', 'nombre','acciones'];
  dataSource: MatTableDataSource<Paciente>;
  cantidad : number;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private service:UsuarioService, private snackBar: MatSnackBar) { }

  ngOnInit() {
  
    this.service.listarMenusPageable(0, 10).subscribe(data => {
      let pacientes = JSON.parse(JSON.stringify(data)).content;
      this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;
      this.dataSource = new MatTableDataSource(pacientes);
      this.dataSource.sort = this.sort;

    })
    this.service.mensaje.subscribe(data => {        
      this.snackBar.open(data, 'Aviso', { duration: 2000 });
    }); 
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }



  mostrarMas(e: any){
    console.log(e);
    this.service.listarMenusPageable(e.pageIndex, e.pageSize).subscribe(data => {
      console.log(data);
      let pacientes = JSON.parse(JSON.stringify(data)).content;
      this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;

      this.dataSource= new MatTableDataSource(pacientes);
      
      this.dataSource.sort = this.sort;
      
    });
  }
}
