import { PacienteService } from './../../_service/paciente.service';
import { Paciente } from './../../_model/paciente';
import { Component, OnInit, ViewChild } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import * as decode from 'jwt-decode';

import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';
import {  TOKEN_NAME } from './../../_shared/var.constant';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls:  ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  lista: Paciente[] = [];
  username:String;
  roles=[];


  constructor() { }

  ngOnInit() {
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    console.log(access_token);

    let tk = JSON.parse(sessionStorage.getItem(TOKEN_NAME));
    let decodedToken = decode(tk.access_token);
    this.username=decodedToken.user_name;
    this.roles=decodedToken.authorities;
  }

  
}
