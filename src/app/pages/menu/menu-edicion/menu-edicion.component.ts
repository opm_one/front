import { MenuService } from './../../../_service/menu.service';
import { Menu } from './../../../_model/menu';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-menu-edicion',
  templateUrl: './menu-edicion.component.html',
  styleUrls: ['./menu-edicion.component.css']
})
export class MenuEdicionComponent implements OnInit {

  id: number;
  menu: Menu;
  form: FormGroup;
  edicion: boolean = false;

  constructor(private menuService: MenuService, private route: ActivatedRoute, private router: Router) {
    this.menu = new Menu();

    this.form = new FormGroup({
      'id': new FormControl(0),
      'url': new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(70)]),
      'nombre': new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(70)]),
      'icono': new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(8)])
    });
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = params['id'] != null;
      this.initForm();
    });
  }

  private initForm() {
    if (this.edicion) {
      this.menuService.listarMenuPorId(this.id).subscribe(data => {
        let id = data.idMenu;
        let url = data.url;
        let nombre = data.nombre;
        let icono = data.icono;


        this.form = new FormGroup({
          'id': new FormControl(id),
          'url': new FormControl(url),
          'nombre': new FormControl(nombre),
          'icono': new FormControl(icono)
        });
      });
    }
  }

  operar() {
    this.menu.idMenu = this.form.value['id'];
    this.menu.url = this.form.value['url'];
    this.menu.nombre = this.form.value['nombre'];
    this.menu.icono = this.form.value['icono'];


    if (this.edicion) {
      //update
      this.menuService.modificar(this.menu).subscribe(data => {
        this.menuService.listarMenus().subscribe(menus => {
          this.menuService.menuCambio.next(menus);
          this.menuService.mensaje.next('Se modificó');
        });
      });
    } else {
      //insert
      this.menuService.registrar(this.menu).subscribe(data => {
        this.menuService.listarMenus().subscribe(menus => {
          this.menuService.menuCambio.next(menus);
          this.menuService.mensaje.next('Se registró');
        });
      });
    }

    this.router.navigate(['menu'])
  }

}
