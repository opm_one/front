import { SignoVital } from './../../../_model/signoVital';
import { SignoVitalService } from './../../../_service/signo-vital.service';
import { PacienteService } from './../../../_service/paciente.service';

import { Paciente } from './../../../_model/paciente';

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { map, startWith } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-paciente-edicion',
  templateUrl: './signo-vital-edicion.component.html',
  styleUrls: ['./signo-vital-edicion.component.css']
})
export class SignoVitalEdicionComponent implements OnInit {

  id: number;
  signoVital: SignoVital;
  form: FormGroup;
  edicion: boolean = false;
  myControlPaciente: FormControl = new FormControl();
  filteredOptions: Observable<any[]>;
  pacientes: Paciente[] = [];
  pacienteSeleccionado: Paciente;

  fechaSeleccionada: Date = new Date();
  maxFecha: Date = new Date();
  

  constructor(private service: SignoVitalService, private route: ActivatedRoute, private router: Router,
    private pacienteService: PacienteService) {
    this.signoVital = new SignoVital();

    this.form = new FormGroup({
      'paciente': this.myControlPaciente,
      'fechaSeleccionada':new FormControl(new Date())    ,  
      'id': new FormControl(0),
      
      'temperatura': new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(70)]),
      'pulso': new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(70)]),
      'ritmoCardiaco': new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(8)])
    });
  }

  displayFn(val: Paciente) {
    return val ? `${val.nombres} ${val.apellidos}` : val;
  }


  filter(val: any) {
    if (val != null && val.idPaciente > 0) {
      return this.pacientes.filter(option =>
        option.nombres.toLowerCase().includes(val.nombres.toLowerCase()) || option.apellidos.toLowerCase().includes(val.apellidos.toLowerCase()) || option.dni.includes(val.dni));
    } else {
      return this.pacientes.filter(option =>
        option.nombres.toLowerCase().includes(val.toLowerCase()) || option.apellidos.toLowerCase().includes(val.toLowerCase()) || option.dni.includes(val));
    }
  }

  ngOnInit() {
    this.listarPacientes();
    

    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = params['id'] != null;
      this.initForm();
    });


    this.filteredOptions = this.myControlPaciente.valueChanges
    .pipe(
      startWith(null),
      map(val => this.filter(val))
    );


  }

  private initForm() {
    if (this.edicion) {
      this.service.listarSignoVitalPorId(this.id).subscribe(data => {
        let id = data.idSignoVitale;
        let temperatura = data.temperatura;
        let pulso = data.pulso;
        let ritmoCardiaco = data.ritmoCardiaco;
        
        this.myControlPaciente=new  FormControl(data.paciente);
        this.pacienteSeleccionado=data.paciente;

        this.form = new FormGroup({
          'id': new FormControl(id),
          'temperatura': new FormControl(temperatura),
          'pulso': new FormControl(pulso),
          'ritmoCardiaco': new FormControl(ritmoCardiaco),
          'paciente': this.myControlPaciente,
          'fechaSeleccionada':new FormControl(data.fecha)

        });
      });
    }
  }

  operar() {
    this.signoVital.idSignoVitale = this.form.value['id'];
    this.signoVital.temperatura = this.form.value['temperatura'];
    this.signoVital.ritmoCardiaco = this.form.value['ritmoCardiaco'];
    this.signoVital.pulso = this.form.value['pulso'];
    this.signoVital.paciente = this.form.value['paciente'];
    var tzoffset = (this.fechaSeleccionada).getTimezoneOffset() * 60000; 
    var localISOTime = (new Date(Date.now() - tzoffset)).toISOString()    
    this.signoVital.fecha = localISOTime;
    
    if (this.edicion) {
      //update
      this.service.modificar(this.signoVital).subscribe(data => {
        this.service.listarSignoVital().subscribe(pacientes => {
          this.service.signoVitalCambio.next(pacientes);
          this.service.mensaje.next('Se modificó');
        });
      });
    } else {
      //insert
      this.service.registrar(this.signoVital).subscribe(data => {
        this.service.listarSignoVital().subscribe(pacientes => {
          this.service.signoVitalCambio.next(pacientes);
          this.service.mensaje.next('Se registró');
        });
      });
    }

    this.router.navigate(['signovital'])
  }


  listarPacientes() {
    this.pacienteService.listarPacientes().subscribe(data => {
      this.pacientes = data;
    });
  }

  seleccionarPaciente(e) {
    this.pacienteSeleccionado = e.option.value;
  }

}
