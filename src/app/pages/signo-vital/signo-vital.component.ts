import { SignoVitalService } from './../../_service/signo-vital.service';
import { Paciente } from './../../_model/paciente';
import { SignoVital } from './../../_model/signoVital';

import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-paciente',
  templateUrl: './signo-vital.component.html',
  styleUrls: ['./signo-vital.component.css']
})
export class SignoVitalComponent implements OnInit {

  lista: SignoVital[] = [];
  displayedColumns = ['idSignoVitale', 'paciente', 'temperatura',  'pulso',  'ritmoCardiaco', 'acciones'];
  dataSource: MatTableDataSource<SignoVital>;
  cantidad : number;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  constructor(private service: SignoVitalService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.service.signoVitalCambio.subscribe(data => {
      console.log(data);
      this.lista = data;
      this.dataSource = new MatTableDataSource(this.lista);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

      this.service.mensaje.subscribe(data => {        
        this.snackBar.open(data, 'Aviso', { duration: 2000 });
      });      
    });

    this.service.listarSignoVitalPageable(0, 10).subscribe(data => {
      let signosVitales = JSON.parse(JSON.stringify(data)).content;
      this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;
      this.dataSource = new MatTableDataSource(signosVitales);
      this.dataSource.sort = this.sort;
   

    })
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  eliminar(idPaciente: number) {
    this.service.eliminar(idPaciente).subscribe(data => {
      this.service.listarSignoVital().subscribe(data => {
        this.lista = data;
        this.dataSource = new MatTableDataSource(this.lista);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
    }
    );
  }

  mostrarMas(e: any){
    console.log(e);
    this.service.listarSignoVitalPageable(e.pageIndex, e.pageSize).subscribe(data => {
      console.log(data);
      let pacientes = JSON.parse(JSON.stringify(data)).content;
      this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;

      this.dataSource= new MatTableDataSource(pacientes);
      
      this.dataSource.sort = this.sort;
      
    });
  }
}
