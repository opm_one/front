import { PerfilComponent } from './pages/perfil/perfil.component';
import { EspecialComponent } from './pages/consulta/especial/especial.component';
import { TokenComponent } from './login/recuperar/token/token.component';
import { RecuperarComponent } from './login/recuperar/recuperar.component';
import { ReporteComponent } from './pages/reporte/reporte.component';
import { BuscarComponent } from './pages/consulta/buscar/buscar.component';
import { GuardService } from './_service/guard.service';
import { LoginComponent } from './login/login.component';
import { MedicoComponent } from './pages/medico/medico.component';
import { ConsultaComponent } from './pages/consulta/consulta.component';
import { EspecialidadEdicionComponent } from './pages/especialidad/especialidad-edicion/especialidad-edicion.component';
import { EspecialidadComponent } from './pages/especialidad/especialidad.component';
import { ExamenEdicionComponent } from './pages/examen/examen-edicion/examen-edicion.component';
import { ExamenComponent } from './pages/examen/examen.component';
import { PacienteComponent } from './pages/paciente/paciente.component';
import { SignoVitalComponent } from './pages/signo-vital/signo-vital.component';
import { SignoVitalEdicionComponent } from './pages/signo-vital/signo-vital-edicion/signo-vital-edicion.component';


import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PacienteEdicionComponent } from './pages/paciente/paciente-edicion/paciente-edicion.component';
import { MenuEdicionComponent } from './pages/menu/menu-edicion/menu-edicion.component';
import { MenuComponent } from './pages/menu/menu.component';

import { RolToMenuAssingComponent } from './pages/rol-to-menu/rol-to-menu-assing/rol-to-menu-assing.component';
import { RolToMenuComponent } from './pages/rol-to-menu/rol-to-menu.component';


import { MenuToUserAssingComponent } from './pages/menu-to-user/menu-to-user-assing/menu-to-user-assing.component';
import { MenuToUserComponent } from './pages/menu-to-user/menu-to-user.component';

import { Not403Component } from './pages/not403/not403.component';

const routes: Routes = [
  { path: 'not-403', component: Not403Component },
  { path: 'perfil', component: PerfilComponent },
  
  { path: 'consulta', component: ConsultaComponent, canActivate: [GuardService] },
  { path: 'consulta-especial', component: EspecialComponent, canActivate: [GuardService] },
  { path: 'buscar', component: BuscarComponent, canActivate: [GuardService]},
  { path: 'medico', component: MedicoComponent, canActivate: [GuardService] },
  {
    path: 'signovital', component: SignoVitalComponent, children: [
      { path: 'nuevo', component: SignoVitalEdicionComponent },
      { path: 'edicion/:id', component: SignoVitalEdicionComponent },
    ], canActivate: [GuardService]
  },
  {
    path: 'paciente', component: PacienteComponent, children: [
      { path: 'nuevo', component: PacienteEdicionComponent },
      { path: 'edicion/:id', component: PacienteEdicionComponent },
    ], canActivate: [GuardService]
  },

  {
    path: 'roltomenu', component: RolToMenuComponent, children: [
      { path: 'asignar/:id', component: RolToMenuAssingComponent },
    ], canActivate: [GuardService]
  },

  {
    path: 'menutouser', component: MenuToUserComponent, children: [
      { path: 'asignar/:id', component: MenuToUserAssingComponent },
    ], canActivate: [GuardService]
  },


  {
    path: 'menu', component: MenuComponent, children: [
      { path: 'nuevo', component: MenuEdicionComponent },
      { path: 'edicion/:id', component: MenuEdicionComponent },
    ], canActivate: [GuardService]
  },
  {
    path: 'especialidad', component: EspecialidadComponent, children: [
      { path: 'nuevo', component: EspecialidadEdicionComponent },
      { path: 'edicion/:id', component: EspecialidadEdicionComponent },
    ], canActivate: [GuardService]
  },
  {
    path: 'examen', component: ExamenComponent, children: [
      { path: 'nuevo', component: ExamenEdicionComponent },
      { path: 'edicion/:id', component: ExamenEdicionComponent },
    ], canActivate: [GuardService]
  },
  { path: 'reporte', component: ReporteComponent, canActivate: [GuardService] },
  { path: 'login', component: LoginComponent },
  {
    path: 'recuperar', component: RecuperarComponent, children: [
        { path: ':token', component: TokenComponent }
    ]
},
  { path: '', redirectTo: 'login', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
