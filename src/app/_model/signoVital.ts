import { Paciente } from "./paciente";

export class SignoVital{
    public idSignoVitale: number;
    public paciente: Paciente;    
    public temperatura: number;   
    public pulso: number;    
    public ritmoCardiaco: number;    
    public fecha: string;
    
}